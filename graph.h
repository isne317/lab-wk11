#include <iostream>
#include <list>
using namespace std;

class Node {
    public:
        Node() { edge = vert = NULL; }
        Node(const int& el, Node *a = 0, Node *b = 0){ key = el; edge = a; vert = b; }
        Node *edge, *vert;
        int key;
};

class  Graph {
    public:
        Graph() { root = 0; }
        ~Graph(){ clear(); };
        void clear() { clear(root); root = 0; }
        void pushInList(int nodeSize, list<int> aryList);
        void showList(int nodeSize);    //Function shows Adjacent List of this graph
        bool Multig(int nodeSize);      
        bool Pseudog(int nodeSize);     
        bool Directg(int nodeSize);     
        bool Weightg(int nodeSize);     
        bool Completeg(int nodeSize);   
        
        int find(int nodeSize, int i);
        int uni(int nodeSize, int i,int j);
        void Prim(int nodeSize, int** ary);	//Prim's Algorithm
        void Kruskal(int nodeSize, int** ary);	//Kruskal's Algorithm

    private:
        void clear(Node *p);
        Node *root;
};

void Graph::clear(Node *p){
	if (p != 0){clear(p->edge); clear(p->vert); delete p;}
}

void Graph::pushInList(int nodeSize,list<int> aryList){
	Node *index,*p;
	index = new Node();
	root = index;
	for(int i = 0; i < nodeSize; i++){
		index->key = i+1;
		p = index;
		for(int j = 0; j < nodeSize; j++){
			if(aryList.front()!=0) {
				Node *Edge,*Vertice;
				Edge = new Node();
				Vertice = new Node();
				p->edge = Edge;
				Edge->key = j+1;
				Vertice->key = aryList.front();
				Edge->vert = Vertice;
				p = p->edge;
			}

			aryList.pop_front();
		}
		Node *tmp;
		tmp = new Node();
		index->vert = tmp;
		index = tmp;
	}
}

void Graph::showList(int nodeSize){
	Node *tmp = root;
	char index;
	for(int i = 0; i < nodeSize; i++){
		index = i+65;
		cout<<index;
		Node *p = tmp->edge,*vt;
		index = tmp->key+64;
		vt = p->vert;
		while(vt != NULL){
			index = p->key+64;
			cout<<" -> "<<index<<vt->key<<" ";
			vt = vt->vert;
			if(vt == NULL && p->edge != NULL){
				p = p->edge;
				vt = p->vert;
			}
		}
		tmp = tmp->vert;
		if(i != nodeSize-1) {cout<<"\n|\n";}
	}
}

bool Graph::Multig(int nodeSize){
	Node *tmp = root,*p,*chk;
	int counter = 0;
	for(int i = 0; i < nodeSize; i++){
		p = tmp->edge;
		chk = p->vert;
		while(chk != NULL){
			counter++;
			chk = chk->vert;
			if(counter>1){return true;}
            else if(chk == NULL && p->edge != NULL){
				p = p->edge;
				chk = p->vert;
				counter = 0;
			}
		}
		tmp = tmp->vert;
	}
	return false;
}

bool Graph::Pseudog(int nodeSize){
	Node *tmp = root,*p;
	char index;
	while(tmp != NULL){
		p = tmp->edge;
		while(p != NULL){
			if(p->key == tmp->key){index = 64+p->key; return true;}
			p = p->edge;
		}
		tmp = tmp->vert;
	}
	return false;
}

bool Graph::Directg(int nodeSize){
	Node *tmp = root,*p,*chk;
	for(int i = 0; i < nodeSize; i++){
		p = tmp->edge;
		if(p->key != tmp->key){
			chk = root;
			for(int j = 1; j < p->key; j++) {chk = chk->vert;}
			for(int j = 0; j < tmp->key; j++) {chk = chk->edge;}
			if(chk->vert->key != p->vert->key) {return true;}
		}
	}
	return false;
}

bool Graph::Weightg(int nodeSize){
	Node *tmp = root,*p;
	while(tmp != NULL){
		p = tmp->edge;
		while(p != NULL){
			if(p->vert->key == -1){return true;}
			p = p->edge;
		}
		tmp = tmp->vert;
	}
	return false;
}

bool Graph::Completeg(int nodeSize){
	Node *tmp = root,*p;
	int counter = 0;
	for(int i = 0; i < nodeSize; i++){
		p = tmp->edge;
		while(p != NULL){
			if(p->key != tmp->key){counter++;}
			p = p->edge;
		}
		tmp = tmp->vert;
	}
	if(counter == nodeSize*(nodeSize-1))return true; else return false;
}

void Graph::Prim(int nodeSize, int** ary){
    int selected[nodeSize],i,j,noEdg;
    int min,x,y;
    int infi = 5000;

    cout<<"\nPrim's Algorithm." << endl;
    for(i=0;i<nodeSize;i++){
    	selected[i]=false;
	}
	
    selected[0]=true;
    noEdg=0;

    while(noEdg < nodeSize-1){
        min=infi;
        for(i=0;i<nodeSize;i++){
            if(selected[i]==true){
                for(j=0;j<nodeSize;j++){
                    if(selected[j]==false){
                        if(min > ary[i][j]){
                            min=ary[i][j]; x=i; y=j;
                        }
                    }
                }
            }
        }
        selected[y]=true;
        cout << endl << x+1 << " --> " << y+1;
        noEdg=noEdg+1;
    }
}

int Graph::find(int nodeSize, int i){
	int parent[nodeSize];
	while(parent[i]){
		i=parent[i];
	}
	return i;
}

int Graph::uni(int nodeSize, int i,int j){
    int parent[nodeSize];
	if(i!=j){
		parent[j]=i;
		return 1;
	}
	return 0;
}

void Graph::Kruskal(int nodeSize, int** ary){
	cout << endl << "--------------------------------";
    cout << endl << endl << "Kruskal's Algorithm'" << endl;
    int noEdg=1,n,a,u,b,v,mincost=0,min;
    int cost[nodeSize][nodeSize];
	while(noEdg < n){
		for(int i=1,min=999;i<=n;i++){
			for(int j=1;j <= n;j++){
				if(cost[i][j] < min){
					min=cost[i][j];
					a=u=i;
					b=v=j;
				}
			}
		}
		u=find(nodeSize, u);
		v=find(nodeSize, v);

		if(uni(nodeSize, u, v)){
			cout << noEdg++ << " edge (" << a << "," << b <<") = " << min << endl;
			mincost +=min;
		}
		cost[a][b]=cost[b][a]=999;
	}
	cout << endl << "Minimum cost = " << mincost << endl;

}
